shopping_list = ['oranges', 'water', 'chicken', 'potatoes', 'washing liquid']
print(shopping_list[0])
print(shopping_list[2])
print(shopping_list[-1])
print(shopping_list[-2])
shopping_list.append('lemons')
print(shopping_list)
print(shopping_list[-1])

number_of_items_to_buy = len(shopping_list)
print(number_of_items_to_buy)

first_three_shopping_items = shopping_list[0:3]
print(first_three_shopping_items)

animal = {
    "name": "Burek",
    "kind": "dog",
    "age": 7,
    "male": True
}
dog_age = animal["age"]
print("Dog age:", dog_age)
dog_name = animal["name"]
print(dog_name)

animal["age"] = 10
print(animal)
animal["owner"] = "Staszek"
print(animal)
