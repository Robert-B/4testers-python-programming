friend_first_name = "Maciej"
friend_age = 47
friend_number_of_pets = 0
friend_has_driving_license = True
friendship_length_in_years = 7

print("My friend's name:", friend_first_name)
print("My friend's age:", friend_age)
print("My friend's number of pets:", friend_number_of_pets)
print("My friend has driving license:", friend_has_driving_license)
print("How many years we've been friends:", friendship_length_in_years)

