def square_given_number(input_number):
    return input_number ** 2


def volume_of_cuboid(a, b, c):
    return a * b * c


def convert_celsius_to_fahrenheit(temp_in_celsius):
    return 9 / 5 * temp_in_celsius + 32


if __name__ == '__main__':
    zero_squared = square_given_number(0)
    sixteen_squared = square_given_number(16)
    float_squared = square_given_number(2.55)
    print("Square of 0 is", zero_squared)
    print("Square of 16 is", sixteen_squared)
    print("Square of 2.55 is", float_squared)
    # Exercise 2
    volume_result = volume_of_cuboid(3, 5, 7)
    print(volume_result)
    # Exercise 3
    fahrenheit_temperature = convert_celsius_to_fahrenheit(20)
    print(fahrenheit_temperature)
